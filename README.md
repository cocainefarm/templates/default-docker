# Docker Container Project
Container build for `{{ .project }}`

## Quickstart
```
docker run {{ .namespace }}/{{ .project }}:latest
```

## Environment Variables
`SET_THING`: explain thing. (Default DEFAULT)

## Ports
`80`: http

## Volumes
`/srv/volume`: bla bla


## Project Links
[GitLab](https://gitlab.com/cocainefarm/{{ .project }})
[GitHub](https://github.com/cocainefarm/{{ .project }})
[DockerHub](https://hub.docker.com/r/cocainefarm/{{ .project }})
